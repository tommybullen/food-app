import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Pressable, ScrollView } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Tracker() {
  const [tracked, setTracked] = useState([]);
  const [sorted, setSorted] = useState([]);
  const [selected, setSelected] = useState(-1);
  const daily = {
    calories: 2000,
    carbohydrate: 260,
    sugars: 90,
    fat: 70,
    saturatedFat: 20,
    protein: 50,
    fibre: 30,
    salt: 6,
  };
  useEffect(() => {
    const getTracked = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem("@FoodApp:tracked");
        console.log("jsonValue", jsonValue);
        jsonValue != null && setTracked(JSON.parse(jsonValue));
        console.log("TRACKED", JSON.parse(jsonValue));
      } catch (e) {
        console.log(e);
      }
    };
    getTracked();
  }, []);

  useEffect(() => {
    const sortDates = () => {
      let tempSorted = [...sorted];
      tracked.map((item) => {
        console.log(item.time.slice(0, 10));
        let idx = tempSorted.findIndex(
          (ele) => ele.date === item.time.slice(0, 10)
        );
        if (idx === -1) {
          tempSorted.push({ date: item.time.slice(0, 10), meals: [item] });
        } else {
          tempSorted[idx].meals.push(item);
        }
      });
      console.log("SORTED", tempSorted);
      setSorted(tempSorted);
    };
    sortDates();
  }, [tracked]);

  const select = (idx) => {
    selected === idx ? setSelected(-1) : setSelected(idx);
  };

  return (
    <View style={styles.main}>
      <ScrollView>
        <View style={styles.top}>
          {sorted.map((day, idx) => {
            let month = day.date.slice(5, 7);
            let strMonth = "";
            month === "01"
              ? (strMonth = "January")
              : month === "02"
              ? (strMonth = "February")
              : month === "03"
              ? (strMonth = "March")
              : month === "04"
              ? (strMonth = "April")
              : month === "05"
              ? (strMonth = "May")
              : month === "06"
              ? (strMonth = "June")
              : month === "07"
              ? (strMonth = "July")
              : month === "08"
              ? (strMonth = "August")
              : month === "09"
              ? (strMonth = "September")
              : month === "10"
              ? (strMonth = "October")
              : month === "11"
              ? (strMonth = "November")
              : month === "12"
              ? (strMonth = "December")
              : (strMonth = "none");

            let str = "";
            let dayNutrition = {
              calories: 0,
              carbohydrate: 0,
              sugars: 0,
              fat: 0,
              saturatedFat: 0,
              protein: 0,
              fibre: 0,
              salt: 0,
            };
            day.meals.map((meal) => {
              str = str + meal.name + ", ";
              meal.recipe.map((ele) => {
                dayNutrition.calories +=
                  (ele.food.foodNutrients.find(
                    (nutr) =>
                      nutr.nutrientId === 1008 || nutr.nutrientId === 2047
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.carbohydrate +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1005
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.sugars +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 2000
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.fat +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1004
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.saturatedFat +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1258
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.protein +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1003
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.fibre +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1079
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100;
                dayNutrition.salt +=
                  (ele.food.foodNutrients.find(
                    (nutr) => nutr.nutrientId === 1093
                  ).value *
                    ele.amount) /
                  meal.portions /
                  100000;
              });
            });
            return (
              <Pressable
                onPress={() => select(idx)}
                style={{ flex: 1 }}
                key={day.date}
              >
                <View
                  style={
                    selected !== idx
                      ? styles.daycontainer
                      : {
                          ...styles.daycontainer,
                          backgroundColor: "rgba(245, 71, 73, 0.5)",
                        }
                  }
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text style={styles.daytext}>
                      {day.date.slice(8, 10) +
                        " " +
                        strMonth +
                        " " +
                        day.date.slice(0, 4)}
                    </Text>
                    <Text style={styles.daytext}>
                      {dayNutrition.calories.toFixed(0)}kcal
                    </Text>
                  </View>
                  <Text style={styles.list}>
                    {str.slice(0, str.length - 2)}
                  </Text>

                  {selected == idx && (
                    <View style={styles.tablecontainer}>
                      <View style={styles.table}>
                        <Text style={styles.label}>Calories:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.calories.toFixed(0)} kcal
                        </Text>
                        <Text style={styles.value}>
                          {(
                            (dayNutrition.calories / daily.calories) *
                            100
                          ).toFixed(0)}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Carbohydrate:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.carbohydrate.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {(
                            (dayNutrition.carbohydrate / daily.carbohydrate) *
                            100
                          ).toFixed(0)}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Sugars:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.sugars.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {((dayNutrition.sugars / daily.sugars) * 100).toFixed(
                            0
                          )}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Fat:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.fat.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {((dayNutrition.fat / daily.fat) * 100).toFixed(0)}%
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Saturated fat:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.saturatedFat.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {(
                            (dayNutrition.saturatedFat / daily.saturatedFat) *
                            100
                          ).toFixed(0)}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Protein:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.protein.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {(
                            (dayNutrition.protein / daily.protein) *
                            100
                          ).toFixed(0)}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Fibre:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.fibre.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {((dayNutrition.fibre / daily.fibre) * 100).toFixed(
                            0
                          )}
                          %
                        </Text>
                      </View>
                      <View style={styles.table}>
                        <Text style={styles.label}>Salt:</Text>
                        <Text style={styles.value}>
                          {dayNutrition.salt.toFixed(1)}g
                        </Text>
                        <Text style={styles.value}>
                          {((dayNutrition.salt / daily.salt) * 100).toFixed(0)}%
                        </Text>
                      </View>
                    </View>
                  )}
                </View>
              </Pressable>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fde7e7",
  },
  top: {
    width: "100%",
    backgroundColor: "#fde7e7",
    padding: "5%",
  },
  daycontainer: {
    backgroundColor: "#f54749",
    borderRadius: 3,
    marginTop: 5,
    color: "white",
    padding: 5,
  },
  daytext: {
    color: "white",
    fontSize: 18,
    padding: 5,
  },
  mealscontainer: {
    borderRadius: 3,
    marginTop: 5,
    padding: 5,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "auto",
  },
  meals: {
    backgroundColor: "#f26801",
    borderRadius: 3,
    marginRight: 3,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2,
    marginBottom: 3,
    maxWidth: "60%",
  },
  mealstext: {
    color: "white",
    fontSize: 12,
  },
  tablecontainer: {
    padding: 5,
    backgroundColor: "#f26801",
    borderRadius: 3,
    width: "100%",
  },
  table: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  label: {
    color: "white",
    fontSize: 14,
    flex: 2,
  },
  value: {
    color: "white",
    fontSize: 14,
    flex: 1,
    textAlign: "center",
  },
  list: {
    fontFamily: "Mansalva_400Regular",
    fontSize: 16,
    color: "white",
    marginBottom: 3,
  },
});
