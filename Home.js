import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Pressable,
  Image,
  ScrollView,
  Platform,
  Picker,
} from "react-native";
import axios from "axios";
// import { Picker } from "@react-native-picker/picker";
import RNPickerSelect from "react-native-picker-select";

export default function Home(props) {
  const [input, setInput] = useState("");
  const [results, setResults] = useState([]);
  const [selected, setSelected] = useState(0);
  const [amount, setAmount] = useState(100);
  const [foods, setFoods] = useState(props.fud);
  const [total, setTotal] = useState(0);
  const [display, setDisplay] = useState("search");
  const [pressed, setPressed] = useState(-1);
  const [message, setMessage] = useState("");

  useEffect(() => {
    console.log("results:", results);
  }, [results]);

  const search = async () => {
    setDisplay("loading");
    try {
      const res = await axios.get(
        `https://api.nal.usda.gov/fdc/v1/foods/search?query=${input}&pageSize=80&dataType=Foundation,Survey%20%28FNDDS%29,SR%20Legacy&api_key=azjLS4ayZthNolVSbkYiTrXG04HMkfLLRtfFqTXz`
      );
      console.log(res);
      console.log("RES", res.data.foods);
      let filteredRes = res.data.foods.filter((item) => {
        return (
          item.foodNutrients.some(
            (ele) => ele.nutrientId == 1008 || ele.nutrientId == 2047
          ) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1005) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 2000) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1004) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1258) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1003) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1079) &&
          item.foodNutrients.some((ele) => ele.nutrientId == 1093)
        );
      });
      console.log("FILTERED RES", filteredRes);
      filteredRes.length < 1
        ? setMessage("No results found!")
        : setMessage("Select the closest match");
      setResults(filteredRes);
      setDisplay("picker");
    } catch (err) {
      console.log(err);
      setDisplay("search");
    }
  };

  const cancel = () => {
    setResults([]);
    setSelected(0);
    setDisplay("search");
    setMessage("");
  };

  const submit = () => {
    if (results.length > 1) {
      let tempFoods = [...foods];
      console.log(results[selected]);
      tempFoods.push({ food: results[selected], amount: amount });
      setFoods(tempFoods);
      setResults([]);
      setSelected(0);
      setAmount(100);
      setMessage("");
      setDisplay("search");
      props.getData(tempFoods);
    }
  };

  useEffect(() => {
    console.log("hello");
    let tempTotal = 0;
    foods.forEach((item) => {
      tempTotal +=
        (item.food.foodNutrients.find(
          (ele) => ele.nutrientId === 1008 || ele.nutrientId === 2047
        ).value *
          item.amount) /
        100;
    });
    setTotal(tempTotal);
  }, [foods]);

  const press = (idx) => {
    pressed === idx ? setPressed(-1) : setPressed(idx);
  };

  const remove = (idx) => {
    let tempFoods = [...foods];
    tempFoods.splice(idx, 1);
    setFoods(tempFoods);
    props.getData(tempFoods);
    setPressed(-1);
  };

  const clear = () => {
    setFoods([]);
    props.getData([]);
  };

  const toNutrition = () => {
    if (foods.length < 1) {
      setMessage("Add a food to continue");
    } else {
      setMessage("");
      props.jumpTo("second");
    }
  };

  return (
    <View style={styles.main}>
      <ScrollView style={{ width: "100%" }}>
        <Text style={styles.title}>Create a new meal...</Text>
        <View style={styles.top}>
          <View style={styles.topflex}>
            {display === "search" ? (
              <View style={styles.wrapper}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <TextInput
                    onChangeText={(text) => setInput(text)}
                    style={styles.pickerwrapper}
                    onSubmitEditing={search}
                    placeholder="Search for a food"
                  />
                  <Pressable style={styles.iconwrapper} onPress={search}>
                    <Image
                      style={styles.search}
                      source={require("./imgs/search.png")}
                    />
                  </Pressable>
                </View>
              </View>
            ) : display === "loading" ? (
              <View style={styles.loadingwrapper}>
                <Image source={require("./imgs/loading.gif")} />
              </View>
            ) : (
              <View style={styles.wrapper}>
                <View
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <View style={styles.pickerwrapper}>
                    <RNPickerSelect
                      style={styles.picker}
                      placeholder={{}}
                      onValueChange={(itemValue, itemIndex) => {
                        setSelected(itemIndex);
                        message !== "" && setMessage("");
                      }}
                      value={results[selected]?.description}
                      items={results.map(
                        (ele) =>
                          (ele = {
                            label: ele.description,
                            value: ele.description,
                          })
                      )}
                    />
                  </View>
                  <Pressable onPress={cancel} style={styles.iconwrapper}>
                    <Image
                      style={styles.search}
                      source={require("./imgs/cancel.png")}
                    />
                  </Pressable>
                </View>
              </View>
            )}
            {Platform.OS === "ios" ? (
              <View style={{ ...styles.enteramount, paddingTop: 11 }}>
                <RNPickerSelect
                  style={styles.amountpicker}
                  placeholder={{}}
                  onValueChange={(itemValue) => {
                    setAmount(Number(itemValue));
                  }}
                  value={`${amount}`}
                  items={[...Array(2001).keys()]
                    .filter((x) => x % 5 === 0)
                    .map(
                      (ele) =>
                        (ele = {
                          label: `${ele}g`,
                          value: `${ele}`,
                        })
                    )}
                />
              </View>
            ) : (
              <View style={styles.enteramount}>
                <Picker
                  style={styles.amountpicker}
                  selectedValue={amount}
                  onValueChange={(itemValue) => setAmount(itemValue)}
                >
                  {[...Array(2001).keys()]
                    .filter((x) => x % 5 === 0)
                    .map((item) => {
                      return (
                        <Picker.Item
                          key={item}
                          label={item.toString()}
                          value={item}
                        />
                      );
                    })}
                </Picker>
                <Text style={{ color: "black", marginLeft: -15 }}>g</Text>
              </View>
            )}
          </View>
          <View
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={styles.message}>{message}</Text>
            <Pressable
              style={({ pressed }) => [
                { backgroundColor: pressed ? "#f54749" : "#f26801" },
                styles.button,
              ]}
              onPress={submit}
            >
              <Text style={styles.buttontext}>Add</Text>
            </Pressable>
          </View>

          {foods.map((item, idx) => {
            return (
              <Pressable onPress={() => press(idx)} key={item.food.fdcId}>
                <View style={pressed === idx ? styles.table2 : styles.table}>
                  <Text style={{ ...styles.list, flex: 3 }}>
                    {item.food.description}
                  </Text>
                  <Text style={{ ...styles.list, flex: 1 }}>
                    {item.amount}g
                  </Text>
                  <Text style={{ ...styles.list, flex: 1.2 }}>
                    {(
                      (item.food.foodNutrients.find(
                        (ele) =>
                          ele.nutrientId === 1008 || ele.nutrientId === 2047
                      ).value *
                        item.amount) /
                      100
                    ).toFixed(0) + " "}
                    kcal
                  </Text>
                  {pressed === idx && (
                    <Pressable
                      style={styles.deletewrapper}
                      onPress={() => remove(idx)}
                    >
                      <Image
                        style={styles.delete}
                        source={require("./imgs/cancel.png")}
                      />
                    </Pressable>
                  )}
                </View>
              </Pressable>
            );
          })}
          {total > 0 && (
            <View style={styles.totalcontainer}>
              <Text style={styles.total}>Total: {total.toFixed(0)} kcal</Text>
            </View>
          )}
        </View>
        <StatusBar style="auto" />
      </ScrollView>
      <View style={styles.completebuttonwrapper}>
        <Pressable
          style={({ pressed }) => [
            { backgroundColor: pressed ? "#f54749" : "#f26801" },
            styles.completebutton,
          ]}
          onPress={clear}
        >
          <Text style={styles.buttontext}>CLEAR ALL</Text>
        </Pressable>
        <Pressable
          style={({ pressed }) => [
            { backgroundColor: pressed ? "#f54749" : "#f26801" },
            styles.completebutton,
          ]}
          onPress={toNutrition}
        >
          <Text style={styles.buttontext}>&#10003; COMPLETE</Text>
        </Pressable>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fde7e7",
  },
  top: {
    width: "100%",
    backgroundColor: "#fde7e7",
    padding: "5%",
  },
  title: {
    fontSize: 38,
    color: "#f54749",
    fontFamily: "Mansalva_400Regular",
    textAlign: "center",
    marginTop: 10,
  },
  topflex: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
    marginTop: 0,
  },
  wrapper: {
    width: "95%",
    flex: 3,
  },

  enteramount: {
    flex: 1.2,
    backgroundColor: "white",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 3,
    justifyContent: "center",
  },
  pickerwrapper: {
    width: "80%",
    borderWidth: 1,
    borderColor: "white",
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginRight: 5,
    backgroundColor: "white",
    color: "black",
    borderRadius: 3,
  },
  loadingwrapper: {
    width: "95%",
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 3,
  },
  picker: {
    width: "100%",
  },
  amountpicker: {
    color: "black",
    width: "100%",
  },
  button: {
    paddingLeft: 15,
    paddingRight: 15,
    height: 30,
    marginTop: 5,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
    width: "auto",
  },
  completebuttonwrapper: {
    width: "95%",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: "5%",
    marginBottom: "5%",
  },
  completebutton: {
    paddingLeft: 15,
    paddingRight: 15,
    height: 40,
    marginTop: 10,
    marginLeft: 5,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
    width: "35%",
  },
  buttontext: {
    fontSize: 14,
    color: "white",
    textTransform: "uppercase",
  },
  list: {
    fontSize: 17,
    color: "white",
    paddingTop: 4,
    paddingLeft: 4,
  },
  table: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "#f54749",
    marginTop: 5,
    padding: 3,
    borderRadius: 3,
    alignItems: "center",
  },
  table2: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "rgba(245, 71, 73, 0.5)",
    marginTop: 5,
    padding: 3,
    borderRadius: 3,
    alignItems: "center",
  },
  totalcontainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  total: {
    textAlign: "right",
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginTop: 10,
    backgroundColor: "#f54749",
    width: "auto",
    borderRadius: 3,
    padding: 5,
  },
  search: {
    height: 20,
    width: 20,
  },
  iconwrapper: {
    padding: 5,
    backgroundColor: "#f26801",
    borderRadius: 5,
  },
  deletewrapper: {
    flex: 1,
    backgroundColor: "#f54749",
    maxWidth: 30,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    padding: 3,
    borderRadius: 3,
    marginRight: 5,
  },
  delete: {
    height: 20,
    width: 20,
    padding: 0,
    margin: 0,
  },
  message: {
    fontFamily: "Mansalva_400Regular",
    fontSize: 18,
    color: "#f26801",
  },
});

///Yellow: "#ffeb3b"
///Blue: "#2196f3"

//NewYellow: #fbb32a
//lighter yellow? : #fec65e
//NewOrange: #ec5803
//newneworange: #f26801
//NewRed: #f54749
//Newbackground: #fde7e7
