import React, { useState } from "react";
import { View, useWindowDimensions } from "react-native";
import Home from "./Home.js";
import Nutrition from "./Nutrition.js";
import Recipes from "./Recipes.js";
import Tracker from "./Tracker.js";
import Constants from "expo-constants";
import { useFonts, Mansalva_400Regular } from "@expo-google-fonts/mansalva";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";

import AppLoading from "expo-app-loading";

export default function TabViewExample() {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: "first", title: "Home" },
    { key: "second", title: "Nutrition" },
    { key: "third", title: "Tracker" },
    { key: "fourth", title: "Favourites" },
  ]);

  const [fud, setFud] = useState([]);

  const getData = (data) => {
    console.log("DATA", data);
    setFud(data);
  };

  let [fontsLoaded] = useFonts({
    Mansalva_400Regular,
  });

  const FirstRoute = () => <Home getData={getData} fud={fud} />;

  const SecondRoute = () => <Nutrition fud={fud} />;

  const ThirdRoute = () => <Tracker />;

  const FourthRoute = () => <Recipes />;

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: "white" }}
      style={{ backgroundColor: "#f26801" }}
      tabStyle={{ padding: 0 }}
      labelStyle={{ fontSize: 12 }}
    />
  );

  // const renderScene = ({ route, jumpTo }) => {
  //   switch (route.key) {
  //     case "first":
  //       return <Home getData={getData} fud={fud} jumpTo={jumpTo} />;
  //     case "second":
  //       return <Nutrition fud={fud} jumpTo={jumpTo} />;
  //     case "third":
  //       return <Tracker jumpTo={jumpTo} />;
  //     case "fourth":
  //       return <Recipes />;
  //   }
  // };

  const renderScene = SceneMap({
    first: (props) => <Home getData={getData} fud={fud} {...props} />,
    second: SecondRoute,
    third: ThirdRoute,
    fourth: FourthRoute,
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        renderTabBar={renderTabBar}
        style={{
          paddingTop: Constants.statusBarHeight,
          backgroundColor: "#f26801",
        }}
      />
    );
  }
}
