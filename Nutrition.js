import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Pressable,
  Image,
  ScrollView,
  TextInput,
  Picker,
} from "react-native";
// import { Picker } from "@react-native-picker/picker";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Checkbox } from "react-native-paper";
import RNPickerSelect from "react-native-picker-select";

export default function Nutrition(props) {
  console.log("PROPS", props);
  const [foods, setFoods] = useState(props.fud);
  const [nutrition, setNutrition] = useState({
    calories: 0,
    carbohydrate: 0,
    sugars: 0,
    fat: 0,
    saturatedFat: 0,
    protein: 0,
    fibre: 0,
    salt: 0,
  });

  const [name, setName] = useState("");
  const [selected, setSelected] = useState(-1);
  const [recipes, setRecipes] = useState([]);
  const [message, setMessage] = useState("");
  const [portions, setPortions] = useState(1);
  const [checked, setChecked] = useState(true);
  const [tracked, setTracked] = useState([]);

  const [daily, setDaily] = useState({
    calories: 2000,
    carbohydrate: 260,
    sugars: 90,
    fat: 70,
    saturatedFat: 20,
    protein: 50,
    fibre: 30,
    salt: 6,
  });

  const select = (idx) => {
    selected === idx ? setSelected(-1) : setSelected(idx);
  };

  useEffect(() => {
    let tempNutrition = { ...nutrition };
    console.log(foods);
    foods.forEach((item) => {
      let calories =
        (item.food.foodNutrients.find(
          (ele) => ele.nutrientId === 1008 || ele.nutrientId === 2047
        ).value *
          item.amount) /
        100;
      tempNutrition.calories += calories;
      let carboydrate =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1005).value *
          item.amount) /
        100;
      tempNutrition.carbohydrate += carboydrate;
      let sugars =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 2000).value *
          item.amount) /
        100;
      tempNutrition.sugars += sugars;
      let fat =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1004).value *
          item.amount) /
        100;
      tempNutrition.fat += fat;
      let saturatedFat =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1258).value *
          item.amount) /
        100;
      tempNutrition.saturatedFat += saturatedFat;
      let protein =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1003).value *
          item.amount) /
        100;
      tempNutrition.protein += protein;
      let fibre =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1079).value *
          item.amount) /
        100;
      tempNutrition.fibre += fibre;
      let salt =
        (item.food.foodNutrients.find((ele) => ele.nutrientId === 1093).value *
          item.amount) /
        100000;
      tempNutrition.salt += salt;
    });
    setNutrition(tempNutrition);
  }, [foods]);

  useEffect(() => {
    const getRecipes = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem("@FoodApp:recipes");
        jsonValue != null && setRecipes(JSON.parse(jsonValue));
      } catch (err) {
        console.log(err);
      }
    };
    const getTracked = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem("@FoodApp:tracked");
        jsonValue != null && setTracked(JSON.parse(jsonValue));
      } catch (err) {
        console.log(err);
      }
    };
    getRecipes();
    getTracked();
  }, []);

  const save = async () => {
    if (name === "") {
      setMessage("Please give your recipe a name");
      setTimeout(() => setMessage(""), 8000);
    } else {
      let tempRecipes = [...recipes];
      tempRecipes.push({ name: name, recipe: foods, portions: portions });
      try {
        const jsonRecipes = JSON.stringify(tempRecipes);
        console.log("jsonRecipes", jsonRecipes);
        await AsyncStorage.setItem("@FoodApp:recipes", jsonRecipes);
        setMessage("Recipe saved to favourites");
        setTimeout(() => setMessage(""), 8000);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const add = async () => {
    console.log("adding");
    if (name === "") {
      setMessage("Please give your recipe a name");
      setTimeout(() => setMessage(""), 8000);
    } else {
      let tempTracked = [...tracked];
      console.log("tempTracked149", tempTracked);
      tempTracked.push({
        name: name,
        recipe: foods,
        portions: portions,
        time: new Date(),
      });
      try {
        const jsonTracked = JSON.stringify(tempTracked);
        console.log("jsonTracked", jsonTracked);
        await AsyncStorage.setItem("@FoodApp:tracked", jsonTracked);
        setMessage("Meal added to the tracker");
        setTimeout(() => setMessage(""), 8000);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <View style={styles.main}>
      <ScrollView styles={{ width: "100%" }}>
        <View style={styles.top}>
          <View style={styles.foodcontainer}>
            {foods.map((item, idx) => {
              return (
                <Pressable
                  key={item.food.fdcId}
                  style={selected === idx ? styles.selected : styles.foodview}
                  onPress={() => select(idx)}
                >
                  <View>
                    <Text style={styles.foodtext}>
                      {item.amount}g {item.food.description}
                    </Text>
                  </View>
                </Pressable>
              );
            })}
          </View>
          <Pressable onPress={() => select(-1)}>
            <View
              style={
                selected === -1 ? styles.selectedtitle : styles.foodviewtitle
              }
            >
              <Text
                style={{
                  ...styles.foodtext,
                  fontSize: 14,
                }}
              >
                All ingredients
              </Text>
            </View>
          </Pressable>
          <View style={{ paddingLeft: "5%" }}>
            <View style={styles.flex}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Mansalva_400Regular",
                    color: "#f26801",
                    fontSize: 18,
                    marginRight: 10,
                  }}
                >
                  No. portions:
                </Text>
                {Platform.OS === "ios" ? (
                  <View style={{ ...styles.enteramount, paddingTop: 11 }}>
                    <RNPickerSelect
                      style={styles.amountpicker}
                      placeholder={{}}
                      onValueChange={(itemValue) => {
                        setPortions(Number(itemValue));
                      }}
                      value={`${portions}`}
                      items={[...Array(21).keys()]
                        .splice(1, 20)
                        .filter((x) => x % 5 === 0)
                        .map(
                          (ele) =>
                            (ele = {
                              label: `${ele}`,
                              value: `${ele}`,
                            })
                        )}
                    />
                  </View>
                ) : (
                  <View style={styles.enteramount}>
                    <Picker
                      style={styles.amountpicker}
                      selectedValue={portions}
                      onValueChange={(itemValue) => setPortions(itemValue)}
                    >
                      {[...Array(21).keys()].splice(1, 20).map((item) => {
                        return (
                          <Picker.Item
                            key={item}
                            label={item.toString()}
                            value={item}
                          />
                        );
                      })}
                    </Picker>
                  </View>
                )}
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Mansalva_400Regular",
                    color: "#f26801",
                    fontSize: 18,
                    marginLeft: 40,
                  }}
                >
                  Per portion
                </Text>
                <Checkbox
                  status={checked ? "checked" : "unchecked"}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                  color="#f26801"
                />
              </View>
            </View>
          </View>
          {selected === -1 ? (
            <View style={styles.tablecontainer}>
              <View style={styles.table}>
                <Text style={styles.label}>Calories:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.calories / portions).toFixed(0)
                    : nutrition.calories.toFixed(0)}{" "}
                  kcal
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.calories / daily.calories) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Carbohydrate:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.carbohydrate / portions).toFixed(1)
                    : nutrition.carbohydrate.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.carbohydrate / daily.carbohydrate) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Sugars:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.sugars / portions).toFixed(1)
                    : nutrition.sugars.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.sugars / daily.sugars) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Fat:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.fat / portions).toFixed(1)
                    : nutrition.fat.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.fat / daily.fat) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Saturated fat:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.saturatedFat / portions).toFixed(1)
                    : nutrition.saturatedFat.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.saturatedFat / daily.saturatedFat) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Protein:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.protein / portions).toFixed(1)
                    : nutrition.protein.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.protein / daily.protein) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Fibre:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.fibre / portions).toFixed(1)
                    : nutrition.fibre.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.fibre / daily.fibre) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Salt:</Text>
                <Text style={styles.value}>
                  {checked
                    ? (nutrition.salt / portions).toFixed(1)
                    : nutrition.salt.toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    ((nutrition.salt / daily.salt) * 100) /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
            </View>
          ) : (
            <View style={styles.tablecontainer}>
              <View style={styles.table}>
                <Text style={styles.label}>Calories:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) =>
                        ele.nutrientId === 1008 || ele.nutrientId === 2047
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(0) + " "}
                  kcal
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) =>
                        ele.nutrientId === 1008 || ele.nutrientId === 2047
                    ).value *
                      foods[selected].amount) /
                    daily.calories /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Carbohydrate:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1005
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1005
                    ).value *
                      foods[selected].amount) /
                    daily.carbohydrate /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Sugars:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 2000
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 2000
                    ).value *
                      foods[selected].amount) /
                    daily.sugars /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Fat:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1004
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1004
                    ).value *
                      foods[selected].amount) /
                    daily.fat /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Saturated fat:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1258
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1258
                    ).value *
                      foods[selected].amount) /
                    daily.saturatedFat /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Protein:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1003
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1003
                    ).value *
                      foods[selected].amount) /
                    daily.protein /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Fibre:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1079
                    ).value *
                      foods[selected].amount) /
                    100 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1079
                    ).value *
                      foods[selected].amount) /
                    daily.fibre /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
              <View style={styles.table}>
                <Text style={styles.label}>Salt:</Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1093
                    ).value *
                      foods[selected].amount) /
                    100000 /
                    (checked ? portions : 1)
                  ).toFixed(1)}
                  g
                </Text>
                <Text style={styles.value}>
                  {(
                    (foods[selected].food.foodNutrients.find(
                      (ele) => ele.nutrientId === 1093
                    ).value *
                      foods[selected].amount) /
                    daily.salt /
                    1000 /
                    (checked ? portions : 1)
                  ).toFixed(0)}
                  %
                </Text>
              </View>
            </View>
          )}

          <TextInput
            onChangeText={(text) => setName(text)}
            onSubmitEditing={save}
            placeholder="Name your recipe"
            style={styles.input}
          />
          <View style={styles.buttoncontainer}>
            <Pressable
              style={({ pressed }) => [
                { backgroundColor: pressed ? "#f54749" : "#f26801" },
                styles.button,
              ]}
              onPress={save}
            >
              <Text style={styles.buttontext}>Save to favourites</Text>
            </Pressable>
            <Pressable
              style={({ pressed }) => [
                { backgroundColor: pressed ? "#f54749" : "#f26801" },
                styles.button,
              ]}
              onPress={add}
            >
              <Text style={styles.buttontext}>Add to tracker</Text>
            </Pressable>
          </View>
          <Text style={styles.message}>{message}</Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fde7e7",
  },
  top: {
    width: "100%",
    backgroundColor: "#fde7e7",
    padding: "5%",
  },
  tablecontainer: {
    width: "100%",
    alignItems: "center",
    marginTop: 10,
    backgroundColor: "#f54749",
    borderRadius: 3,
    paddingTop: 10,
    paddingBottom: 15,
  },
  table: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginTop: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  label: {
    color: "white",
    fontSize: 17,
    flex: 2,
  },
  value: {
    color: "white",
    fontSize: 17,
    flex: 1.2,
    textAlign: "center",
  },
  foodcontainer: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  foodview: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "#f54749",
    marginTop: 5,
    marginRight: 5,
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
    paddingRight: 5,
    maxWidth: "60%",
  },
  selected: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "rgba(245, 71, 73, 0.5)",
    marginTop: 5,
    marginRight: 5,
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
    paddingRight: 5,
    maxWidth: "60%",
  },
  foodviewtitle: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "#f54749",
    marginTop: 5,
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
    paddingRight: 5,
  },
  selectedtitle: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 10,
    backgroundColor: "rgba(245, 71, 73, 0.5)",
    marginTop: 5,
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
    paddingRight: 5,
  },
  foodtext: {
    fontSize: 12,
    color: "white",
    margin: "auto",
    paddingTop: 10,
    // display: "inline-block",
  },

  buttoncontainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
  },
  input: {
    backgroundColor: "white",
    height: 40,
    marginTop: 10,
    paddingTop: 0,
    paddingLeft: 8,
    borderRadius: 3,
  },
  button: {
    paddingLeft: 5,
    paddingRight: 5,
    height: 35,
    marginTop: 5,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
    width: "49%",
  },
  buttontext: {
    fontSize: 14,
    color: "white",
    textTransform: "uppercase",
  },
  message: {
    fontFamily: "Mansalva_400Regular",
    fontSize: 18,
    color: "#f54749",
  },
  flex: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",

    marginTop: 10,
    width: "100%",
  },
  enteramount: {
    backgroundColor: "white",
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 3,
    justifyContent: "center",
    width: 80,
  },
  amountpicker: {
    color: "black",
    width: "100%",
  },
  checkbox: {},
});

//NewYellow: #fbb32a
//NewOrange: #ec5803
//newneworange: #f26801
//NewRed: #f54749
//Newbackground: #fde7e7
