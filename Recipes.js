import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Pressable,
  Image,
  ScrollView,
  TextInput,
} from "react-native";
import { Checkbox } from "react-native-paper";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Recipes() {
  const [recipes, setRecipes] = useState([]);
  const [selected, setSelected] = useState(-1);
  const [checked, setChecked] = useState(true);
  const [daily, setDaily] = useState({
    calories: 2000,
    carbohydrate: 260,
    sugars: 90,
    fat: 70,
    saturatedFat: 20,
    protein: 50,
    fibre: 30,
    salt: 6,
  });

  useEffect(() => {
    const getRecipes = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem("@FoodApp:recipes");
        console.log("jsonValue", jsonValue);
        jsonValue != null && setRecipes(JSON.parse(jsonValue));
        console.log("RECIPES", JSON.parse(jsonValue));
      } catch (e) {
        console.log(e);
      }
    };
    getRecipes();
  }, []);

  const select = (idx) => {
    selected === idx ? setSelected(-1) : setSelected(idx);
  };

  const remove = async (idx) => {
    let tempRecipes = [...recipes];
    tempRecipes.splice(idx, 1);
    try {
      const jsonRecipes = JSON.stringify(tempRecipes);
      console.log("jsonRecipes", jsonRecipes);
      await AsyncStorage.setItem("@FoodApp:recipes", jsonRecipes);
      setRecipes(tempRecipes);
      setSelected(-1);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.main}>
      <ScrollView styles={{ width: "100%" }}>
        <View style={styles.top}>
          {recipes.map((item, idx) => {
            let nutrients = {
              calories: 0,
              carbohydrate: 0,
              sugars: 0,
              fat: 0,
              saturatedFat: 0,
              protein: 0,
              fibre: 0,
              salt: 0,
            };
            item.recipe.forEach((ele) => {
              nutrients.calories +=
                (ele.food.foodNutrients.find(
                  (nutr) => nutr.nutrientId === 1008 || nutr.nutrientId === 2047
                ).value *
                  ele.amount) /
                100;
              nutrients.carbohydrate +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1005)
                  .value *
                  ele.amount) /
                100;
              nutrients.sugars +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 2000)
                  .value *
                  ele.amount) /
                100;
              nutrients.fat +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1004)
                  .value *
                  ele.amount) /
                100;
              nutrients.saturatedFat +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1258)
                  .value *
                  ele.amount) /
                100;
              nutrients.protein +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1003)
                  .value *
                  ele.amount) /
                100;
              nutrients.fibre +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1079)
                  .value *
                  ele.amount) /
                100;
              nutrients.salt +=
                (ele.food.foodNutrients.find((nutr) => nutr.nutrientId === 1093)
                  .value *
                  ele.amount) /
                100000;
            });
            return selected === idx ? (
              <Pressable
                onPress={() => select(idx)}
                style={{ flex: 1 }}
                key={idx}
              >
                <View style={styles.ingredientscontainer}>
                  <View style={styles.titlewrapper}>
                    <Text style={styles.recipename}>{item.name}</Text>

                    <Pressable
                      onPress={() => remove(idx)}
                      style={styles.deletewrapper}
                    >
                      <Image
                        style={styles.delete}
                        source={require("./imgs/cancel.png")}
                      />
                    </Pressable>
                  </View>
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Mansalva_400Regular",
                        color: "white",
                        fontSize: 16,
                        marginRight: 10,
                      }}
                    >
                      Portions: {item.portions}
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginLeft: 30,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Mansalva_400Regular",
                          color: "white",
                          fontSize: 16,
                          marginRight: 10,
                        }}
                      >
                        Per portion:
                      </Text>
                      <Checkbox
                        status={checked ? "checked" : "unchecked"}
                        onPress={() => {
                          setChecked(!checked);
                        }}
                        color="white"
                      />
                    </View>
                  </View>
                  {item.recipe.map((ele) => {
                    console.log("HERE", ele.food.description, ele.amount);
                    return (
                      <View key={ele.food.fdcId} style={styles.ingredients}>
                        <Text style={styles.ingredientstext}>
                          {ele.amount}g {ele.food.description}
                        </Text>
                      </View>
                    );
                  })}
                  <View style={styles.tablecontainer}>
                    <View style={styles.table}>
                      <Text style={styles.label}>Calories:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.calories / item.portions).toFixed(0)
                          : nutrients.calories.toFixed(0)}{" "}
                        kcal
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.calories / daily.calories) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Carbohydrate:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.carbohydrate / item.portions).toFixed(1)
                          : nutrients.carbohydrate.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.carbohydrate / daily.carbohydrate) *
                            100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Sugars:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.sugars / item.portions).toFixed(1)
                          : nutrients.sugars.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.sugars / daily.sugars) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Fat:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.fat / item.portions).toFixed(1)
                          : nutrients.fat.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.fat / daily.fat) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Saturated fat:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.saturatedFat / item.portions).toFixed(1)
                          : nutrients.saturatedFat.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.saturatedFat / daily.saturatedFat) *
                            100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Protein:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.protein / item.portions).toFixed(1)
                          : nutrients.protein.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.protein / daily.protein) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Fibre:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.fibre / item.portions).toFixed(1)
                          : nutrients.fibre.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.fibre / daily.fibre) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                    <View style={styles.table}>
                      <Text style={styles.label}>Salt:</Text>
                      <Text style={styles.value}>
                        {checked
                          ? (nutrients.salt / item.portions).toFixed(1)
                          : nutrients.salt.toFixed(1)}
                        g
                      </Text>
                      <Text style={styles.value}>
                        {(
                          ((nutrients.salt / daily.salt) * 100) /
                          (checked ? item.portions : 1)
                        ).toFixed(0)}
                        %
                      </Text>
                    </View>
                  </View>
                </View>
              </Pressable>
            ) : (
              <Pressable onPress={() => select(idx)} key={idx}>
                <View style={styles.recipecontainer}>
                  <Text style={styles.recipetext}>{item.name}</Text>
                  <Text style={styles.calorietext}>
                    {nutrients.calories.toFixed(0)} kcal
                  </Text>
                </View>
              </Pressable>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fde7e7",
  },
  top: {
    width: "100%",
    backgroundColor: "#fde7e7",
    padding: "5%",
  },
  recipecontainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f54749",
    borderRadius: 3,
    marginTop: 5,
    color: "white",
  },
  recipetext: {
    fontFamily: "Mansalva_400Regular",
    color: "white",
    fontSize: 24,
    padding: 5,
  },
  calorietext: {
    color: "white",
    fontSize: 16,
    padding: 5,
  },
  ingredientscontainer: {
    backgroundColor: "rgba(245, 71, 73, 0.5)",
    borderRadius: 3,
    marginTop: 5,
    padding: 5,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "auto",
  },
  recipename: {
    fontSize: 24,
    color: "white",
    marginBottom: 2,
    fontFamily: "Mansalva_400Regular",
  },
  ingredients: {
    backgroundColor: "#f26801",
    borderRadius: 3,
    marginRight: 3,
    paddingLeft: 4,
    paddingRight: 4,
    paddingTop: 2,
    paddingBottom: 2,
    marginBottom: 3,
    maxWidth: "60%",
  },
  ingredientstext: {
    color: "white",
    fontSize: 12,
  },
  tablecontainer: {
    padding: 5,
    backgroundColor: "#f26801",
    borderRadius: 3,
    width: "100%",
  },
  table: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  label: {
    color: "white",
    fontSize: 14,
    flex: 2,
  },
  value: {
    color: "white",
    fontSize: 14,
    flex: 1,
    textAlign: "center",
  },
  titlewrapper: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    height: "auto",
  },
  deletewrapper: {
    backgroundColor: "#f54749",
    borderRadius: 3,
    width: 25,
    height: 25,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  delete: {
    width: 20,
    height: 20,
    resizeMode: "contain",
  },
});
